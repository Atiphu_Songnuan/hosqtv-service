<?php
$data = json_decode(file_get_contents('php://input'), true);
$ip = get_client_ip();
// if ($ip == "172.29.70.129" || $ip == "61.19.201.18") {
$service = $data["service"];
$path = __DIR__ . "/logs/update2his-" . date("Y-m-d") . ".txt";
// @file_put_contents($path, "Access from: " . $ip . PHP_EOL, FILE_APPEND);
// @file_put_contents($path, "Call Service: " . $service . PHP_EOL, FILE_APPEND);
switch ($service) {
    // Clinic-Q Mobile Application
    case "GETOPDCOUNTER":
        $sql = "SELECT
                    A.C_OPD, A.N_OPD
                FROM
                    OPD.Fopdcounter AS A -- GET data from clinicqueue Mobile Application by perid=46773";
        $sql = urlencode($sql);
        $url = "http://192.168.1.33/his/general.php?sql=" . $sql;
        $header = array(
            "http" => [
                "method" => "GET",
                "header" => "User-Agent: Microsoft\r\n",
            ],
        );
        $context = stream_context_create($header);
        $result = iconv("TIS-620", "UTF-8", file_get_contents($url, false, $context));
        $data = explode("\n", $result);
        $table = array();
        foreach ($data as $v) {
            if (strlen($v) > 0) {
                $line = explode("\t", $v);
                array_push($table, $line);
            }
        }
        $jsondata = json_encode($table, true);
        echo $jsondata;
        break;

    case "CHECKISDOCTOR":
        $perid = $data["perid"];
        $sql = "SELECT NAME, SURNAME, SEX FROM STAFF.Medperson WHERE PERID = '" . $perid . "'  -- GET data from clinicqueue Mobile Application by perid=46773";
        $sql = urlencode($sql);
        $url = "http://192.168.1.33/his/general.php?sql=" . $sql;
        $header = array(
            "http" => [
                "method" => "GET",
                "header" => "User-Agent: Microsoft\r\n",
            ],
        );
        $context = stream_context_create($header);
        $result = iconv("TIS-620", "UTF-8", file_get_contents($url, false, $context));
        $data = explode("\n", $result);
        $table = array();
        foreach ($data as $v) {
            if (strlen($v) > 0) {
                $line = explode("\t", $v);
                array_push($table, $line);
            }
        }
        $jsondata = json_encode($table, true);
        echo $jsondata;
        break;
    case "GETDOCTORDATA":
        $perid = $data["perid"];
        $date_d = $data["date_d"];
        $sql = "SELECT A.C_DOCT, C.NAME, C.SURNAME,A.date_d,A.code_b,B.u_name
                FROM OPD.Doctor_day as A
                LEFT JOIN Mydata.Funit as B on A.code_b=B.c_unit
                LEFT JOIN STAFF.Medperson AS C ON A.C_DOCT = C.PERID
                WHERE A.c_doct='" . $perid . "'
                    AND A.date_d = '" . $date_d . "' -- GET data from clinicqueue Mobile Application by perid=46773";
        $sql = urlencode($sql);
        $url = "http://192.168.1.33/his/general.php?sql=" . $sql;
        $header = array(
            "http" => [
                "method" => "GET",
                "header" => "User-Agent: Microsoft\r\n",
            ],
        );
        $context = stream_context_create($header);
        $result = iconv("TIS-620", "UTF-8", file_get_contents($url, false, $context));
        $data = explode("\n", $result);
        $table = array();
        foreach ($data as $v) {
            if (strlen($v) > 0) {
                $line = explode("\t", $v);
                array_push($table, $line);
            }
        }
        $jsondata = json_encode($table, true);
        echo $jsondata;
        break;

    case "GETDOCTORPATIENTQUEUE":
        $date_d = $data["date_d"];
        $code_b = $data["code_b"];
        $c_doct = $data["c_doct"];
        $sql = "SELECT
                    A.hn,
                    A.date_d,
                    A.code_b,
                    time_format(A.date_q,'%H:%i'),
                    A.qtime,
                    time_format(D.qdoct,'%H:%i'),
                    time_format(A.docttime,'%H:%i'),
                    time_format(B.date_close,'%H:%i'),
                    time_format(D.qexitnurse,'%H:%i'),
                    time_format(C.nursetime,'%H:%i'),
                    E.pname,
                    E.psur,
                    E.sex
                FROM OPD.Opdq as A
                LEFT JOIN OPD.Cl_regis as B on A.hn=B.hn
                    AND A.date_d=B.date_cl
                    AND A.code_b=B.code_cl
                    AND A.c_doct=B.c_doct
                LEFT JOIN OPD.DClregis as C on A.hn=C.hn
                    AND A.date_d=C.date_cl
                    AND A.code_b=C.code_cl
                    AND A.c_doct=C.c_doct
                LEFT JOIN OPD.Opd_MQ AS D ON A.HN = D.HN
                    AND A.DATE_D = D.DATE_D
                    AND A.CODE_B = D.CODE_B
                    AND A.C_DOCT = D.C_DOCT
                    -- AND QEXITNURSE = '0000-00-00 00:00:00'
                LEFT JOIN Mydata.Medrec as E on A.hn=E.hn
                WHERE A.date_d='" . $date_d . "'
                    AND A.code_b='" . $code_b . "'
                    AND A.c_doct='" . $c_doct . "'
                    -- AND isnull(A.outtime)
                GROUP BY A.hn
                ORDER BY A.qtime -- GET data from clinicqueue Mobile Application by perid=46773";
        $sql = urlencode($sql);
        $url = "http://192.168.1.33/his/general.php?sql=" . $sql;
        $header = array(
            "http" => [
                "method" => "GET",
                "header" => "User-Agent: Microsoft\r\n",
            ],
        );
        $context = stream_context_create($header);
        $result = iconv("TIS-620", "UTF-8", file_get_contents($url, false, $context));
        $data = explode("\n", $result);
        $table = array();
        foreach ($data as $v) {
            if (strlen($v) > 0) {
                $line = explode("\t", $v);
                array_push($table, $line);
            }
        }
        $jsondata = json_encode($table, true);
        echo $jsondata;
        break;

    case "GETPATIENTINFO":
        $hn = $data["hn"];
        $sql = "SELECT HN, PNAME, PSUR FROM Mydata.Medrec WHERE hn = '" . $hn . "' -- GET data from clinicqueue Mobile Application by perid=46773";
        $sql = urlencode($sql);
        $url = "http://192.168.1.33/his/general.php?sql=" . $sql;
        $header = array(
            "http" => [
                "method" => "GET",
                "header" => "User-Agent: Microsoft\r\n",
            ],
        );
        $context = stream_context_create($header);
        $result = iconv("TIS-620", "UTF-8", file_get_contents($url, false, $context));
        $data = explode("\n", $result);
        $table = array();
        foreach ($data as $v) {
            if (strlen($v) > 0) {
                $line = explode("\t", $v);
                array_push($table, $line);
            }
        }
        $jsondata = json_encode($table);
        echo $jsondata;
        break;

    case "GETPATIENTTIME":
        $curDate = $data["date_d"];
        $clinicCode = $data["code_b"];
        $doctorPerid = $data["c_doct"];
        $hn = $data["hn"];

        $sql = "SELECT HN, DATE_D, CODE_B, C_DOCT
                FROM OPD.Opd_MQ
                WHERE HN = '" . $hn . "'
                AND DATE_D = '" . $curDate . "'
                AND CODE_B = '" . $clinicCode . "'
                AND C_DOCT = '" . $doctorPerid . "' -- GET data from clinicqueue Mobile Application by perid=46773";

        $sql = urlencode($sql);
        $url = "http://192.168.1.33/his/general.php?sql=" . $sql;
        $header = array(
            "http" => [
                "method" => "GET",
                "header" => "User-Agent: Microsoft\r\n",
            ],
        );
        $context = stream_context_create($header);
        $result = iconv("TIS-620", "UTF-8", file_get_contents($url, false, $context));
        $data = explode("\n", $result);
        $table = array();
        foreach ($data as $v) {
            if (strlen($v) > 0) {
                $line = explode("\t", $v);
                array_push($table, $line);
            }
        }
        header("Content-type:application/json");
        $jsondata = json_encode($table);
        $logdata = array(
            "hn" => $hn,
            "date_d" => $curDate,
            "code_b" => $clinicCode,
            "c_doct" => $doctorPerid,
        );
        $newRequestData = date("Y-m-d H:i:s") . "\tservice: GET" . "\tdata: " . trim(preg_replace('/\s/', '', json_encode($logdata))) . PHP_EOL;
        @file_put_contents($path, $newRequestData, FILE_APPEND);

        echo $jsondata;
        break;

    case "INSERTPATIENTTIME":
        $hn = $data["hn"];
        $date_d = $data["date_d"];
        $code_b = $data["code_b"];
        $c_doct = $data["c_doct"];

        $sql = "INSERT INTO OPD.Opd_MQ(HN, DATE_D, CODE_B, C_DOCT, QDOCT) VALUES('" . $hn . "', '" . $date_d . "', '" . $code_b . "', '" . $c_doct . "', NOW())-- INSERT PATIENTTIME data from clinicqueue Mobile Application by perid=46773";
        $sql = urlencode($sql);
        $url = "http://192.168.1.33/his/general.php?sql=" . $sql;
        $header = array(
            "http" => [
                "method" => "GET",
                "header" => "User-Agent: Microsoft\r\n",
            ],
        );
        $context = stream_context_create($header);
        $result = iconv("TIS-620", "UTF-8", file_get_contents($url, false, $context));
        $data = explode("\n", $result);
        $table = array();
        foreach ($data as $v) {
            if (strlen($v) > 0) {
                $line = explode("\t", $v);
                array_push($table, $line);
            }
        }
        header("Content-type:application/json");
        $jsondata = json_encode($table);
        $logdata = array(
            "hn" => $hn,
            "date_d" => $date_d,
            "code_b" => $code_b,
            "c_doct" => $c_doct,
        );
        $newRequestData = date("Y-m-d H:i:s") . "\tservice: INSERT" . "\tdata: " . trim(preg_replace('/\s/', '', json_encode($logdata))) . PHP_EOL;
        @file_put_contents($path, $newRequestData, FILE_APPEND);

        echo $jsondata;
        break;
    case "UPDATEPATIENTTIME":
        $hn = $data["hn"];
        $date_d = $data["date_d"];
        $code_b = $data["code_b"];
        $c_doct = $data["c_doct"];

        $sql = "UPDATE OPD.Opd_MQ SET QEXITNURSE = NOW() WHERE HN = '" . $hn . "' AND DATE_D= '" . $date_d . "' AND CODE_B = '" . $code_b . "' AND C_DOCT = '" . $c_doct . "' -- UPDATE PATIENTTIME data from clinicqueue Mobile Application by perid=46773";
        @file_put_contents($path, $sql . PHP_EOL, FILE_APPEND);
        $sql = urlencode($sql);
        $url = "http://192.168.1.33/his/general.php?sql=" . $sql;
        $header = array(
            "http" => [
                "method" => "GET",
                "header" => "User-Agent: Microsoft\r\n",
            ],
        );
        $context = stream_context_create($header);
        $result = iconv("TIS-620", "UTF-8", file_get_contents($url, false, $context));
        $data = explode("\n", $result);
        @file_put_contents($path, $data . PHP_EOL, FILE_APPEND);
        $table = array();
        foreach ($data as $v) {
            if (strlen($v) > 0) {
                $line = explode("\t", $v);
                array_push($table, $line);
            }
        }
        header("Content-type:application/json");
        $jsondata = json_encode($table);
        echo $jsondata;
        break;

    case "UPDATEQUEUEOUTTIME":
        $hn = $data["hn"];
        $date_d = $data["date_d"];
        $code_b = $data["code_b"];
        $c_doct = $data["c_doct"];
        $outtime = $data["outtime"];

        $sql = "UPDATE OPD.Opdq
                SET OUTTIME = '" . $outtime . "'
                WHERE HN = '" . $hn . "'
                AND DATE_D= '" . $date_d . "'
                AND CODE_B = '" . $code_b . "'
                AND C_DOCT = '" . $c_doct . "' -- UPDATE PATIENTTIME data from clinicqueue Mobile Application by perid=46773";
        @file_put_contents($path, $sql . PHP_EOL, FILE_APPEND);
        $sql = urlencode($sql);
        $url = "http://192.168.1.33/his/general.php?sql=" . $sql;
        $header = array(
            "http" => [
                "method" => "GET",
                "header" => "User-Agent: Microsoft\r\n",
            ],
        );
        $context = stream_context_create($header);
        $result = iconv("TIS-620", "UTF-8", file_get_contents($url, false, $context));
        $data = explode("\n", $result);
        @file_put_contents($path, $data . PHP_EOL, FILE_APPEND);
        $table = array();
        foreach ($data as $v) {
            if (strlen($v) > 0) {
                $line = explode("\t", $v);
                array_push($table, $line);
            }
        }
        header("Content-type:application/json");
        $jsondata = json_encode($table);
        echo $jsondata;
        break;

    case "CLEARPATIENTTIME":
        $hn = $data["hn"];
        $code_b = $data["code_b"];
        $c_doct = $data["c_doct"];
        $sql = "DELETE FROM OPD.Opd_MQ WHERE HN= '" . $hn . "' AND CODE_B = '" . $code_b . "' AND C_DOCT = '" . $c_doct . "'-- DELETE PATIENTTIME data from clinicqueue Mobile Application by perid=46773";
        $sql = urlencode($sql);
        $url = "http://192.168.1.33/his/general.php?sql=" . $sql;
        $header = array(
            "http" => [
                "method" => "GET",
                "header" => "User-Agent: Microsoft\r\n",
            ],
        );
        $context = stream_context_create($header);
        $result = iconv("TIS-620", "UTF-8", file_get_contents($url, false, $context));
        $data = explode("\n", $result);
        $table = array();
        foreach ($data as $v) {
            if (strlen($v) > 0) {
                $line = explode("\t", $v);
                array_push($table, $line);
            }
        }
        header("Content-type:application/json");
        $jsondata = json_encode($table);
        echo $jsondata;
        break;

    // OPDQ-TV
    case "GETCOUNTER":
        $counter = $data["counter"];
        $sql = "SELECT
                    A.C_OPD, A.N_OPD
                FROM
                    OPD.Fopdcounter AS A
                WHERE
                    A.C_OPD = '" . $counter . "' LIMIT 1 -- GET data from Fopdcounter HOSQTV Web by perid=46773";

        $sql = urlencode($sql);
        $url = "http://192.168.1.33/his/general.php?sql=" . $sql;
        $header = array(
            "http" => [
                "method" => "GET",
                "header" => "User-Agent: Microsoft\r\n",
            ],
        );
        $context = stream_context_create($header);
        $result = iconv("TIS-620", "UTF-8", file_get_contents($url, false, $context));
        $data = explode("\n", $result);
        $table = array();
        foreach ($data as $v) {
            if (strlen($v) > 0) {
                $line = explode("\t", $v);
                array_push($table, $line);
            }
        }
        $jsondata = json_encode($table);
        @file_put_contents($path, "Service: " . $service . " COUNTER: " . $counter . " " . $jsondata . PHP_EOL, FILE_APPEND);
        echo $jsondata;
        break;
    case "GETOPDQ":
        $counter = $data["counter"];
        // Original by P'Komane
        // $sql = "SELECT DISTINCT A.c_doct,t_doct
        //             FROM OPD.Opdq as A
        //             LEFT JOIN Mydata.Appoint as B on A.hn=B.hn
        //             AND A.date_d=B.date_ap
        //             AND A.code_b=B.code_ap
        //             LEFT JOIN OPD.Doctor_day as C on A.c_doct=C.c_doct
        //             WHERE A.date_d=curdate()
        //             AND A.code_b = '" . $cliniccode . "'
        //             AND isnull(outtime)
        //             AND C.date_d=curdate()
        //             ORDER BY C.t_doct,A.c_doct,qtime -- GET data from opdq HOSQTV Web by perid=46773";

        // Latest Update
        // $sql = "SELECT DISTINCT A.c_doct,C.t_doct
        //         FROM OPD.Opdq as A
        //         LEFT JOIN Mydata.Appoint as B on A.hn=B.hn
        //         AND A.date_d=B.date_ap
        //         AND A.code_b=B.code_ap
        //         LEFT JOIN OPD.Doctor_day as C on A.c_doct=C.c_doct
        //         AND A.date_d = C.DATE_D
        //         AND A.code_b = C.CODE_B
        //         LEFT JOIN OPD.Fopdcounter AS D ON A.OPDCOUNTER = D.C_OPD
        //         WHERE A.date_d=curdate()
        //         AND A.code_b = '" . $counter . "'
        //         AND isnull(outtime)
        //         AND C.date_d=curdate()
        //         ORDER BY C.t_doct,A.c_doct,qtime -- GET data from opdq HOSQTV Web by perid=46773";

        $sql = "SELECT DISTINCT A.c_doct,C.t_doct
                FROM OPD.Opdq as A
                LEFT JOIN Mydata.Appoint as B on A.hn=B.hn
                AND A.date_d=B.date_ap
                AND A.code_b=B.code_ap
                LEFT JOIN OPD.Doctor_day as C on A.c_doct=C.c_doct
                AND A.date_d = C.DATE_D
                AND A.code_b = C.CODE_B
                LEFT JOIN OPD.Fopdcounter AS D ON A.OPDCOUNTER = D.C_OPD
                WHERE A.date_d=curdate()
                AND A.OPDCOUNTER = '" . $counter . "'
                AND isnull(A.outtime)
                AND C.date_d=curdate()
                ORDER BY C.t_doct,A.c_doct,qtime -- GET data from opdq HOSQTV Web by perid=46773";

        $sql = urlencode($sql);
        $url = "http://192.168.1.33/his/general.php?sql=" . $sql;
        $header = array(
            "http" => [
                "method" => "GET",
                "header" => "User-Agent: Microsoft\r\n",
            ],
        );
        $context = stream_context_create($header);
        $result = iconv("TIS-620", "UTF-8", file_get_contents($url, false, $context));
        $data = explode("\n", $result);
        $table = array();
        foreach ($data as $v) {
            if (strlen($v) > 0) {
                $line = explode("\t", $v);
                array_push($table, $line);
            }
        }
        $jsondata = json_encode($table);
        @file_put_contents($path, "Service: " . $service . " COUNTER: " . $counter . " " . $jsondata . PHP_EOL, FILE_APPEND);
        echo $jsondata;
        break;

    case "GETSTAFFNAME":
        $perid = $data["perid"];
        $sql = "SELECT concat(name,' ',surname)
                    FROM STAFF.Medperson
                    WHERE perid='" . $perid . "' -- GET STAFF NAME from opdq HOSQTV Web by perid=46773";

        $sql = urlencode($sql);
        $url = "http://192.168.1.33/his/general.php?sql=" . $sql;
        $header = array(
            "http" => [
                "method" => "GET",
                "header" => "User-Agent: Microsoft\r\n",
            ],
        );
        $context = stream_context_create($header);
        $result = iconv("TIS-620", "UTF-8", file_get_contents($url, false, $context));
        $data = explode("\n", $result);
        $table = array();
        foreach ($data as $v) {
            if (strlen($v) > 0) {
                $line = explode("\t", $v);
                array_push($table, $line);
            }
        }
        $jsondata = json_encode($table, true);
        @file_put_contents($path, "Service: " . $service . " COUNTER: " . $counter . " " . $jsondata . PHP_EOL, FILE_APPEND);
        echo $jsondata;
        break;

    case "GETROOMDETAIL":
        $counter = $data["counter"];
        $doctperid = $data["doctperid"];
        $doctname = $data["doctname"];
        // @file_put_contents($path, "Data: " . $cliniccode . "," . $doctperid . "," . $doctname . PHP_EOL, FILE_APPEND);
        $sql = "SELECT DISTINCT '" . $doctname . "' as DName , B.t_doct as Room
                    FROM OPD.Opdq as A
                LEFT JOIN OPD.Doctor_day as B on A.c_doct=B.c_doct
                    AND A.code_b=B.code_b
                    AND A.date_d=B.date_d
                    WHERE A.date_d=curdate()
                    AND A.OPDCOUNTER = '" . $counter . "'
                    AND isnull(outtime)
                    AND A.c_doct='" . $doctperid . "' -- GET STAFF ROOM DETAIL from opdq HOSQTV Web by perid=46773";

        // $sql = "SELECT concat(name,' ',surname)
        //         FROM STAFF.Medperson
        //         WHERE perid='" . $doctperid . "' -- GET STAFF NAME from opdq HOSQTV Web by perid=46773";
        // @file_put_contents($path, "Query: " . $sql . PHP_EOL, FILE_APPEND);

        $sql = urlencode($sql);
        $url = "http://192.168.1.33/his/general.php?sql=" . $sql;
        $header = array(
            "http" => [
                "method" => "GET",
                "header" => "User-Agent: Microsoft\r\n",
            ],
        );
        $context = stream_context_create($header);
        $result = iconv("UTF-8", "TIS-620", file_get_contents($url, false, $context));
        $result = iconv("TIS-620", "UTF-8", $result);
        // @file_put_contents($path, "Result: " . $result . PHP_EOL, FILE_APPEND);
        $data = explode("\n", $result);
        $table = array();
        foreach ($data as $v) {
            if (strlen($v) > 0) {
                $line = explode("\t", $v);
                array_push($table, $line);
            }
        }
        $jsondata = json_encode($table, true);
        echo $jsondata;
        break;
    case "GETPATIENTS":
        $counter = $data["counter"];
        $doctperid = $data["doctperid"];
        $sql = "SELECT qtime,A.hn,C.pname
                FROM OPD.Opdq as A
                LEFT JOIN Mydata.Appoint as B on A.hn=B.hn
                AND A.date_d=B.date_ap
                AND A.code_b=B.code_ap
                LEFT JOIN Mydata.Medrec as C on A.hn=C.hn
                WHERE date_d=curdate()
                AND A.OPDCOUNTER = '" . $counter . "'
                AND isnull(outtime)
                AND A.c_doct='" . $doctperid . "'
                ORDER BY A.c_doct,qtime -- GET PATIENT DATA from opdq HOSQTV Web by perid=46773";
        // @file_put_contents($path, "Query: " . $sql . PHP_EOL, FILE_APPEND);
        $sql = urlencode($sql);
        $url = "http://192.168.1.33/his/general.php?sql=" . $sql;
        $header = array(
            "http" => [
                "method" => "GET",
                "header" => "User-Agent: Microsoft\r\n",
            ],
        );
        $context = stream_context_create($header);
        $result = iconv("TIS-620", "UTF-8", file_get_contents($url, false, $context));
        // $result = iconv("TIS-620", "UTF-8", $result);
        // @file_put_contents($path, "Result: " . $result . PHP_EOL, FILE_APPEND);
        $data = explode("\n", $result);
        $table = array();
        foreach ($data as $v) {
            if (strlen($v) > 0) {
                $line = explode("\t", $v);
                array_push($table, $line);
            }
        }
        $jsondata = json_encode($table, true);
        echo $jsondata;
        break;
    default:
        # code...
        break;
}

function get_client_ip()
{
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP'])) {
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    } else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else if (isset($_SERVER['HTTP_X_FORWARDED'])) {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    } else if (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    } else if (isset($_SERVER['HTTP_FORWARDED'])) {
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    } else if (isset($_SERVER['REMOTE_ADDR'])) {
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    } else {
        $ipaddress = 'UNKNOWN';
    }

    return $ipaddress;
}
