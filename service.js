const express = require("express");
const cors = require("cors");
const https = require("https");
https.globalAgent.maxSockets = 5;

const app = express();
app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.post("/api/counter", (req, res) => {
  let result = [];
  let body = req.body;
  let counter = body.counter;
  //*** ค้นหาคิวตรวจทั้งหมดแยกตามห้องและแพทย์ ***//
  let data = JSON.stringify({
    service: "GETCOUNTER",
    counter: counter,
  });
  let options = {
    host: "his01.psu.ac.th",
    path: "/HosApp/clinicq/update2his.php",
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Content-Length": Buffer.byteLength(data),
    },
    rejectUnauthorized: false,
  };
  let request = https.request(options, (httpsres) => {
    let data = [];
    httpsres.on("data", (chunk) => {
      data = chunk;
    });
    httpsres.on("end", () => {
      let dataJSON = JSON.parse(data);
      if (dataJSON.length != 0) {
        result = {
          statusCode: 200,
          data: "คลินิก" + dataJSON[0][1],
        };
      } else {
        result = {
          statusCode: 200,
          data: "",
        };
      }
      res.send(result);
    });
  });
  request.on("error", function (e) {
    console.log(e.message);
  });
  request.write(data);
  request.end();
});

app.post("/api/allqueue", (req, res) => {
  let result = [];
  let body = req.body;
  let counter = body.counter;
  //*** ค้นหาคิวตรวจทั้งหมดแยกตามห้องและแพทย์ ***//
  let data = JSON.stringify({
    service: "GETOPDQ",
    counter: counter,
  });
  let options = {
    host: "his01.psu.ac.th",
    path: "/HosApp/clinicq/update2his.php",
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Content-Length": Buffer.byteLength(data),
    },
    rejectUnauthorized: false,
  };
  let request = https.request(options, (httpsres) => {
    let data = [];
    httpsres.on("data", (chunk) => {
      data = chunk;
    });
    httpsres.on("end", () => {
      let dataJSON = JSON.parse(data);
      if (dataJSON.length != 0) {
        let promises = [];
        dataJSON.forEach((element) => {
          let doctperid = element[0];
          // *** ค้นหาชื่อแพทย์จาก perid ***//
          promises.push(GetStaffName(doctperid));
        });

        // ใช้ Promise.All
        Promise.all(promises).then((doctdata) => {
          let roompromises = [];
          doctdata.forEach((element) => {
            let doctperid = element[0].perid;
            let doctname = element[0].doctname;
            roompromises.push(GetRoomDetail(doctperid, doctname, counter));
          });
          Promise.all(roompromises).then((roomdata) => {
            let patientspromises = [];
            roomdata.forEach((element) => {
              let doctperid = element[0].perid;
              let doctname = element[0].doctname;
              let room = element[0].room;
              patientspromises.push(
                GetPatients(doctperid, doctname, counter, room)
              );
            });
            Promise.all(patientspromises).then((patientdata) => {
              // res.send(patientdata)
              let finalresult = [];
              patientdata.forEach((element) => {
                finalresult.push(element[0]);
              });
              result = {
                statusCode: 200,
                data: finalresult,
              };
              res.send(result);
            });
          });
        });
      } else {
        console.log("Queue is empty");
        result = {
          statusCode: 200,
          data: "Empty Room",
        };
        res.send(result);
      }
    });
  });
  request.on("error", function (e) {
    console.log(e.message);
  });
  request.write(data);
  request.end();
});

app.post("/api/roomdetail", (req, res) => {
  let body = req.body;
  let doctperid = body.perid;
  let doctname = body.doctname;
  let clinic = body.clinic;

  let roomdata = JSON.stringify({
    service: "GETROOMDETAIL",
    doctperid: doctperid,
    doctname: doctname,
    clinic: clinic,
  });

  console.log(roomdata);

  let roomoptions = {
    host: "his01.psu.ac.th",
    path: "/HosApp/clinicq/update2his.php",
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Content-Length": Buffer.byteLength(roomdata),
    },
    rejectUnauthorized: false,
  };
  let roomrequest = https.request(roomoptions, (roomres) => {
    let roomresponsedata = [];
    roomres.on("data", function (chunk) {
      roomresponsedata = chunk;
    });
    roomres.on("end", function () {
      let roomDataJSON = JSON.parse(roomresponsedata);
      console.log(roomDataJSON);
      res.send(roomDataJSON);
    });
  });
  roomrequest.on("error", function (e) {
    console.log(e.message);
  });
  roomrequest.write(roomdata);
  roomrequest.end();
});

app.post("/api/staffname", (req, res) => {
  let result = [];
  let body = req.body;
  let perid = body.perid;
  let data = JSON.stringify({
    service: "GETSTAFFNAME",
    perid: perid,
  });
  let options = {
    host: "his01.psu.ac.th",
    path: "/HosApp/clinicq/update2his.php",
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Content-Length": Buffer.byteLength(data),
    },
    rejectUnauthorized: false,
  };
  let request = https.request(options, (httpsres) => {
    let data = [];
    httpsres.on("data", function (chunk) {
      data = chunk;
    });
    httpsres.on("end", function () {
      let dataJSON = JSON.parse(data);
      if (dataJSON.length != 0) {
        result = {
          statusCode: 200,
          staffname: dataJSON[0],
        };
      }
      res.send(result);
    });
  });
  request.on("error", function (e) {
    console.log(e.message);
  });
  request.write(data);
  request.end();
});

function GetStaffName(doctperid) {
  let staffdata = JSON.stringify({
    service: "GETSTAFFNAME",
    perid: doctperid,
  });
  let staffoptions = {
    host: "his01.psu.ac.th",
    path: "/HosApp/clinicq/update2his.php",
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Content-Length": Buffer.byteLength(staffdata),
    },
    rejectUnauthorized: false,
  };

  return new Promise((resolve) => {
    let staffrequest = https.request(staffoptions, (staffres) => {
      let staffresponsedata = [];
      staffres.on("data", (chunk) => {
        staffresponsedata = chunk;
      });
      staffres.on("end", () => {
        let staffDataJSON = JSON.parse(staffresponsedata);
        let staffdataArr = [];
        if (staffDataJSON.length != 0) {
          staffDataJSON.forEach((element) => {
            staffdataArr.push({
              perid: doctperid,
              doctname: element[0],
            });
          });
        } else {
          staffdataArr.push({
            perid: doctperid,
            doctname: "",
          });
        }
        resolve(staffdataArr);
      });
    });
    staffrequest.on("error", function (e) {
      console.log(e.message);
    });
    staffrequest.write(staffdata);
    staffrequest.end();
  });
}

function GetRoomDetail(doctperid, doctname, counter) {
  let roomdata = JSON.stringify({
    service: "GETROOMDETAIL",
    doctperid: doctperid,
    doctname: doctname,
    counter: counter,
  });
  let roomoptions = {
    host: "his01.psu.ac.th",
    path: "/HosApp/clinicq/update2his.php",
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Content-Length": Buffer.byteLength(roomdata),
    },
    rejectUnauthorized: false,
  };

  return new Promise((resolve) => {
    let roomrequest = https.request(roomoptions, (roomres) => {
      let roomresponsedata = [];
      roomres.on("data", function (chunk) {
        roomresponsedata = chunk;
      });
      roomres.on("end", function () {
        let roomDataJSON = JSON.parse(roomresponsedata);
        // console.log("Response: ");
        let roomdataArr = [];
        if (roomDataJSON.length != 0) {
          roomDataJSON.forEach((roomelement) => {
            roomdataArr.push({
              perid: doctperid,
              doctname: roomelement[0],
              room: roomelement[1],
              patients: [],
            });
          });
        } else {
          roomdataArr.push({
            perid: doctperid,
            doctname: "",
            room: "",
            patients: [],
          });
        }
        resolve(roomdataArr);
      });
    });
    roomrequest.on("error", function (e) {
      console.log(e.message);
    });
    roomrequest.write(roomdata);
    roomrequest.end();
  });
}

function GetPatients(doctperid, doctname, counter, room) {
  let patientdata = JSON.stringify({
    service: "GETPATIENTS",
    doctperid: doctperid,
    counter: counter,
  });
  let patientoptions = {
    host: "his01.psu.ac.th",
    path: "/HosApp/clinicq/update2his.php",
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Content-Length": Buffer.byteLength(patientdata),
    },
    rejectUnauthorized: false,
  };
  return new Promise((resolve) => {
    let patientrequest = https.request(patientoptions, (patientres) => {
      let patientresponsedata = [];
      patientres.on("data", function (chunk) {
        patientresponsedata = chunk;
      });
      patientres.on("end", function () {
        let patientDataJSON = JSON.parse(patientresponsedata);
        let summarydataArr = [];
        let patientsdataArr = [];
        if (patientDataJSON.length != 0) {
          patientDataJSON.forEach((patientelement) => {
            patientsdataArr.push({
              time: patientelement[0],
              hn: patientelement[1],
              name: patientelement[2],
            });
          });
        } else {
          patientsdataArr.push({
            time: "",
            hn: "",
            name: "",
          });
        }
        summarydataArr.push({
          doctperid: doctperid,
          doctname: doctname,
          room: room,
          patients: patientsdataArr,
        });
        resolve(summarydataArr);
      });
    });
    patientrequest.on("error", function (e) {
      console.log(e.message);
    });
    patientrequest.write(patientdata);
    patientrequest.end();
  });
}

const port = process.env.PORT || 5339;
app.listen(port, "0.0.0.0", () =>
  console.log("Listening on port " + port + "...")
);
